#!/bin/bash
PROCBIN="/usr/sbin/perdition.imap4"
ISTERM="test -t 1";


if find /proc/ -maxdepth 2 -name 'exe' -lname $PROCBIN > /dev/null 2>&1;
then 
	$ISTERM && echo PASS;
	exit 0;
else 
	$ISTERM && echo FAIL;
	exit 1;
fi; 
